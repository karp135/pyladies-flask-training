# -*- coding: utf-8 -*-
from flask import Flask, url_for, render_template, request, flash, redirect
from flask.ext.login import LoginManager
from wtforms import Form, TextField, validators
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from pymongo import *
#import pdb

Base = declarative_base()

users_list = [
{
    'username':'user',
    'password':'user',
},
{
    'username':'admin',
    'password':'admin',
}
]

class AddFilmForm(Form):    
    title = TextField('Tytul')
    genre = TextField('Typ')
    year_created = TextField('Rok produkcji')    
    direction = TextField('Rezyser')

class LoginForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25)])
    password = TextField('Password', [validators.Length(min=4, max=25)])

app = Flask(__name__)


db_client = MongoClient()
db = db_client.flask
collection = db.films

# SQLAlchemy  snippet below

#db_file = create_engine('sqlite:///flask.db', echo=True)
#db_mem = create_engine('sqlite:///:memory:', echo=True)
#conn_file = db_file.connect()
#conn_mem = db_mem.connect()
#db_mem.echo = false
#db_file.echo = false

login_manager = LoginManager()

@app.route("/")
def main():
    movies_list = []
    for item in collection.find():
        movies_list.append(item)

    movies_amount = len(movies_list)

    per_site_arg = request.args.get('per_site')
    if per_site_arg:        
        films_per_site = int(per_site_arg)
    else:
        films_per_site = 3

    site_nr_arg = request.args.get('site_nr')
    if site_nr_arg:        
        site_nr = int(site_nr_arg)
    else:
        site_nr = 1

    start_film_nr = (site_nr - 1) * films_per_site
    end_film_nr = start_film_nr + films_per_site

    possible_sites_amount = movies_amount / films_per_site
    if (movies_amount % films_per_site) > 0:
        possible_sites_amount += 1

    # do zmiany - zbyt duzo parametrow
    return render_template('index.html', movies_list=movies_list[start_film_nr:end_film_nr], possible_sites_amount=possible_sites_amount,per_site=films_per_site, site_nr=site_nr)

@app.route("/details")
def show_details():
    film_title = request.args.get('title')    
    item=collection.find_one({'title' : film_title})
    return render_template('film_details.html', item=item)

@app.route("/add_film", methods=['GET', 'POST'])
def add_film():
    global current_film_id
    form = AddFilmForm(request.form)
    if request.method == 'POST' and form.validate():        
        film = dict(title=form.title.data, type=form.genre.data, year_created=form.year_created.data,
                    direction=form.direction.data, ratings=[], comments=[])
        collection.insert(film)                
        flash('Film added to DB succesfully')    #flash nie dziala??
        return redirect(url_for('main'))    
    return render_template('add_film.html', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():        
        xx=5
    return render_template('login.html')



@app.route("/show_some_text/<text>")
def show_some_text(text):
    return text

if __name__ == "__main__":
    app.secret_key = 'secret key'
    #app.config[]
    app.run(debug=True, use_reloader=True, port=5000)
#    app.run()
